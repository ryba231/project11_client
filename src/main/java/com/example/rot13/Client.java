package com.example.rot13;

import com.example.rot13.model.ResultData;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;

public class Client {
    private JPanel newClientRootPanel;
    private JPanel leftTextPanel;
    private JPanel rightButtonsArea;
    private JLabel codeLabel;
    private JTextField CodeTextField;
    private JTextArea textArea;
    private JTextArea resultTextArea;
    private JButton EncodeButton;
    private JButton DecodeButton;
    private JButton ClearButton;
    private String answer;
    private int resultCode;


    public Client() {
        textArea.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
            }
        });
        ClearButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textArea.setText("");
                resultTextArea.setText("");
                CodeTextField.setText("");
            }
        });
        EncodeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String url = "http://127.0.0.1:8080/api/rot13/encode";
                HttpClientPost(url,textArea.getText());
                resultTextArea.setText(getAnswer());
                CodeTextField.setText(String.valueOf(getResultCode()));

            }
        });
        DecodeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String url = "http://127.0.0.1:8080/api/rot13/decode";
                HttpClientPost(url,textArea.getText());
                resultTextArea.setText(getAnswer());
                CodeTextField.setText(String.valueOf(getResultCode()));

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Client");
        frame.setContentPane(new Client().newClientRootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);

    }


    private void HttpClientPost(String url,String text){

        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost(url);
        Gson gson = new Gson();

        final ResultData resultData = new ResultData(text);
        final String json = gson.toJson(resultData);
        try {
            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept","application/json");
            httpPost.setHeader("Content-type","application/json");
            final CloseableHttpResponse response = client.execute(httpPost);
            resultCode = response.getStatusLine().getStatusCode();

            if (response.getStatusLine().getStatusCode() == 404){
                answer = "Problem with connection server";
            }else if(response.getStatusLine().getStatusCode() == 200){
                final HttpEntity httpEntity = response.getEntity();
                final String jsonAnswer = EntityUtils.toString(httpEntity);
                final ResultData resultData1 = gson.fromJson(jsonAnswer,ResultData.class);

                answer = resultData1.getResult();

            }
            client.close();


        }catch (ConnectException e) {
            System.out.println("Problem with connecting to the server!");
            System.out.println();
        }catch (UnsupportedEncodingException e) {
            System.out.println("Problem with POST Unsupported Encoding!");
            e.printStackTrace();
        }catch (ClientProtocolException e ){
            System.out.println("Problem with POST Client Protocol!");
            e.printStackTrace();
        }catch (IOException e){
            System.out.println("Problem with POST Input Output!");
            e.printStackTrace();
        }


    }

    public String getAnswer() {
        return answer;
    }

    public int getResultCode() {
        return resultCode;
    }
}
