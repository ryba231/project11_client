package com.example.rot13.model;

import java.io.Serializable;

public class ResultData implements Serializable {

    private String result;

    public ResultData (){

    }
    public ResultData(String result){

        this.result = result;
    }

    public String getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "ResultData{" +
                "result='" + result + '\'' +
                '}';
    }
}
